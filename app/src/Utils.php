<?php namespace App;

/**
 * Example class
 *
 * @package Utils
 */
class Utils {

    /**
     * Returns TRUE
     *
     * @return bool
     */
    public static function returnTrue() {
        return TRUE;
    }

}
